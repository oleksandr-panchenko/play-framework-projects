import akka.actor.ActorSystem
import com.ligadigital.concept.controller.util.AuthenticationAction
import com.ligadigital.concept.controller.{ProjectController, UserController}
import com.ligadigital.concept.service.{DefaultProjectService, DefaultUserService, ProjectService, UserService}
import play.api._
import play.api.ApplicationLoader.Context
import play.api.routing.Router
import com.softwaremill.macwire._
import play.api.i18n.I18nComponents
import play.api.mvc.{ControllerComponents, PlayBodyParsers}
import play.filters.HttpFiltersComponents
import router.Routes

import scala.concurrent.ExecutionContext



class MyApplicationLoader extends ApplicationLoader {
  def load(context: ApplicationLoader.Context) = {
    LoggerConfigurator(context.environment.classLoader).foreach {
      _.configure(context.environment)
    }

    new AppComponents(context).application
  }
}


class AppComponents(context: Context) extends BuiltInComponentsFromContext(context) with AppModule with I18nComponents with HttpFiltersComponents {

  lazy val prefix: String = "/"
  lazy val router: Router = wire[Routes]
}

trait AppModule {
  lazy val userController: UserController = wire[UserController]
  lazy val projectController: ProjectController = wire[ProjectController]
  lazy val projectService: ProjectService = wire[DefaultProjectService]
  lazy val userService: UserService = wire[DefaultUserService]
  lazy val authenticationAction: AuthenticationAction = wire[AuthenticationAction]
  def controllerComponents: ControllerComponents
  def playBodyParsers: PlayBodyParsers
  implicit def executionContext: ExecutionContext
  implicit def actorSystem: ActorSystem
}

