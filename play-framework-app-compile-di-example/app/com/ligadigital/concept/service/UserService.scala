package com.ligadigital.concept.service

import com.ligadigital.concept.error.{AppError, ObjectAlreadyExistsError}
import com.ligadigital.concept.message.UserAlreadyExists
import com.ligadigital.concept.model.User
import play.api.Play

import scala.concurrent.{ExecutionContext, Future}

trait UserService {
  def create(name: String, email: String): Future[Either[AppError, User]]
  def findAll(): Future[List[User]]
  def find(userId: Long): Future[Option[User]]
}

class DefaultUserService (implicit ec: ExecutionContext) extends UserService {
  private var users = List(User(1, "user1", "user1@email.com"), User(3, "user3", "user3@email.com"), User(2, "user2", "user2@email.com"))

  override def create(name: String, email: String): Future[Either[AppError, User]] = Future {
    if (users.exists(_.email == email)) {
      Left(ObjectAlreadyExistsError(UserAlreadyExists))
    } else {
      val newUser = User(users.size + 1, name, email)
      users = newUser +: users
      Right(newUser)
    }
  }

  override def find(userId: Long): Future[Option[User]] = Future {
    users.find(_.id == userId)
  }

  override def findAll(): Future[List[User]] = Future(users)
}
