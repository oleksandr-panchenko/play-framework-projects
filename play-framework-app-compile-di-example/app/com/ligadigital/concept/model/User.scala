package com.ligadigital.concept.model

import play.api.libs.json.{Json, Writes}

case class User(id: Long, name: String, email: String)

object User {
  implicit def userWrites: Writes[User] = Json.format[User]
}