package com.ligadigital.concept.model

import java.time.LocalDate

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Writes}

case class Project(id: Long, name: String, authorId: Long, created: LocalDate, budget: Long, startDate: Option[LocalDate])

object Project {
  implicit def userWrites: Writes[Project] = (
    (JsPath \ "id").write[Long] and
      (JsPath \ "name").write[String] and
      (JsPath \ "authorId").write[Long] and
      (JsPath \ "created").write[LocalDate] and
      (JsPath \ "budget").write[Long] and
      (JsPath \ "startDate").write[Option[LocalDate]]
    )(unlift(Project.unapply))
}
