package com.ligadigital.concept.controller

import akka.actor.ActorSystem
import com.ligadigital.concept.controller.util.{AuthenticationAction, ResultConverter}
import com.ligadigital.concept.service.UserService
import play.api.i18n.I18nSupport
import play.api.libs.json.JsValue
import play.api.mvc._

import scala.concurrent.ExecutionContext

class UserController(userService: UserService, authenticationAction: AuthenticationAction, cc: ControllerComponents)(implicit ec: ExecutionContext, actorSystem: ActorSystem) extends AbstractController(cc)
  with ResultConverter
  with I18nSupport {

  def getUsers() = Action.async(parse.empty) { implicit request =>
    userService.findAll()
  }

  def getUser(id: Long) = authenticationAction.async(parse.empty) { implicit request =>
    userService.find(id)
  }

  def createUser(): Action[JsValue] = authenticationAction.async(parse.json) { implicit request =>
    val body = request.body
    val name = (body \ "name").as[String]
    val email = (body \ "email").as[String]
    userService.create(name, email)
  }

  def index = Action {
    Ok("Works")
  }
}
