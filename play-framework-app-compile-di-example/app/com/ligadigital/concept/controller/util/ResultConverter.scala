package com.ligadigital.concept.controller.util

import com.ligadigital.concept.error.{AppError, NotFoundError, ObjectAlreadyExistsError}
import com.ligadigital.concept.message.TextResource
import play.api.http.Status
import play.api.i18n.Messages
import play.api.libs.json.{Json, Writes}
import play.api.mvc.{Result, Results}

import scala.concurrent.{ExecutionContext, Future}

trait ResultConverter {
  implicit def convertEitherToResult[T](o: Future[Either[AppError, T]])(implicit i18n: Messages, writes: Writes[T], ec: ExecutionContext): Future[Result] = o map {
    case Left(NotFoundError(messages)) =>
      Results.NotFound(toJson(Status.NOT_FOUND, messages))
    case Left(ObjectAlreadyExistsError(messages)) =>
      Results.BadRequest(toJson(Status.BAD_REQUEST, messages))
    case Left(e: AppError) =>
      Results.BadRequest(toJson(Status.INTERNAL_SERVER_ERROR, e.messages))
    case Right(obj) =>
      Results.Ok(Json.toJson(obj))
  }

  implicit def listToResult[T](o: Future[List[T]])(implicit writes: Writes[T], ec: ExecutionContext): Future[Result] = o map { obj =>
    Results.Ok(Json.toJson(obj))
  }

  implicit def optionToResult[T](o: Future[Option[T]])(implicit writes: Writes[T], ec: ExecutionContext): Future[Result] = o map { obj =>
    Results.Ok(Json.toJson(obj))
  }

  private def toJson(status: Int, messages: List[TextResource])(implicit i18n: Messages) = {
    Json.obj(
      "status_code" -> status,
      "error_messages" -> messages.map(textRes => i18n(textRes.code))
    )
  }
}