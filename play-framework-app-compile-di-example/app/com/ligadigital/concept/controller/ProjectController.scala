package com.ligadigital.concept.controller

import com.ligadigital.concept.controller.util.{AuthenticationAction, ResultConverter}
import com.ligadigital.concept.service.ProjectService
import play.api.i18n.I18nSupport
import play.api.mvc.{AbstractController, ControllerComponents}

import scala.concurrent.ExecutionContext

class ProjectController(projectService: ProjectService,
                        authenticationAction: AuthenticationAction,
                        cc: ControllerComponents)(implicit ec: ExecutionContext) extends AbstractController(cc) with ResultConverter with I18nSupport {

  def getProjects() = authenticationAction.async(parse.empty) { implicit request =>
    projectService.findAll()
  }

  def getProject(id: Long) = authenticationAction.async(parse.empty) { implicit request =>
    projectService.find(id)
  }

}
