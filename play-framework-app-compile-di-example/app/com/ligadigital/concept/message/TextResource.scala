package com.ligadigital.concept.message

trait TextResource {
  val code: String
}

case object UserNotFound extends TextResource {
  override val code: String = "user.not.found"
}
case object UserAlreadyExists extends TextResource {
  override val code: String = "user.already.exists"
}
case object InvalidEmail extends TextResource {
  override val code: String = "email.format.invalid"
}

case object ProjectNotFound extends TextResource {
  override val code: String = "project.not.found"
}
case object ProjectAlreadyExists extends TextResource {
  override val code: String = "project.already.exists"
}
case object ProjectNameValidation extends TextResource {
  override val code: String = "project.validation.name"
}
case object ProjectBudgetValidation extends TextResource {
  override val code: String = "project.validation.budget"
}