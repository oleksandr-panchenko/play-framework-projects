name := """play-framework-app-compile-di-example"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala, SwaggerPlugin)

scalaVersion := "2.12.5"

libraryDependencies ++= Seq(
  "com.softwaremill.macwire" %% "macros" % "2.3.0"
)

swaggerV3 := true
swaggerDomainNameSpaces := Seq("com.ligadigital.concept.model")
swaggerTarget := new File("./assets")

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"
