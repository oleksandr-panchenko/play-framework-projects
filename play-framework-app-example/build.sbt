name := "play-framework-app-example"

version := "0.1"

scalaVersion := "2.12.5"

lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .settings(
    libraryDependencies ++= Seq(
      guice
    )
  )