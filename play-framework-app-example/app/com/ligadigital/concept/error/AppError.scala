package com.ligadigital.concept.error

import com.ligadigital.concept.message.TextResource

/**
  * @author oleksandr.panchenko@ligadigital.com
  */
sealed trait AppError {
  val messages: List[TextResource]
}

case class NotFoundError(messages: List[TextResource]) extends AppError
object NotFoundError {
  def apply(message: TextResource): NotFoundError = new NotFoundError(List(message))
}

case class ValidationError(messages: List[TextResource]) extends AppError
object ValidationError {
  def apply(message: TextResource): ValidationError = new ValidationError(List(message))
}

case class ObjectAlreadyExistsError(messages: List[TextResource]) extends AppError
object ObjectAlreadyExistsError {
  def apply(message: TextResource): ObjectAlreadyExistsError = new ObjectAlreadyExistsError(List(message))
}


