package com.ligadigital.concept.controller

import com.ligadigital.concept.controller.util.ResultConverter
import com.ligadigital.concept.service.ProjectService
import javax.inject.Inject
import play.api.i18n.I18nSupport
import play.api.mvc.{MessagesAbstractController, MessagesControllerComponents}

import scala.concurrent.ExecutionContext

class ProjectController @Inject()(projectService: ProjectService,
                                  mcc: MessagesControllerComponents)(implicit ec: ExecutionContext) extends MessagesAbstractController(mcc)
                                                                                              with ResultConverter with I18nSupport {

  def getProjects() = Action.async(parse.empty) { implicit request =>
    projectService.findAll()
  }

  def getProject(id: Long) = Action.async(parse.empty) { implicit request =>
    projectService.find(id)
  }

}
