package com.ligadigital.concept.controller.util

import javax.inject.Inject
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Random

case class AuthenticatedRequest[A](userId: Long, request: Request[A]) extends WrappedRequest[A](request)

class AuthenticationAction @Inject()(bp: PlayBodyParsers)(implicit ec: ExecutionContext) extends ActionBuilder[AuthenticatedRequest, Any] {

  override def parser: BodyParser[Any] = bp.anyContent

  override def invokeBlock[A](request: Request[A], block: AuthenticatedRequest[A] => Future[Result]): Future[Result] = {
    val userId = 1 // somehow extract user id from request
    val authenticated = Random.nextBoolean() // do some staff to check if a user authenticated
    if (authenticated) {
      block(AuthenticatedRequest(userId, request))
    } else {
      Future.successful(Results.Unauthorized(""))
    }
  }

  override protected def executionContext: ExecutionContext = ec
}
