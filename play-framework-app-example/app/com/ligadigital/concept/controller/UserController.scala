package com.ligadigital.concept.controller

import com.ligadigital.concept.controller.util.{AuthenticationAction, ResultConverter}
import com.ligadigital.concept.service.UserService
import javax.inject.Inject
import play.api.i18n.I18nSupport
import play.api.mvc.{AbstractController, ControllerComponents}

import scala.concurrent.ExecutionContext

class UserController @Inject()(userService: UserService,
                               authenticatedAction: AuthenticationAction,
                               cc: ControllerComponents)(implicit ec: ExecutionContext) extends AbstractController(cc)
                                                                                            with ResultConverter with I18nSupport {
  def getUsers() = Action.async(parse.empty) { implicit request =>
    userService.findAll()
  }

  def getUser(id: Long) = authenticatedAction.async(parse.empty) { implicit request =>
    userService.find(id)
  }

  def createUser() = authenticatedAction.async(parse.json) { implicit request =>
    val body = request.body
    val name = (body \ "name").as[String]
    val email = (body \ "email").as[String]
    userService.create(name, email)
  }
}
