package com.ligadigital.concept.model

import play.api.libs.json.{JsPath, Writes}
import play.api.libs.functional.syntax._


case class User(id: Long, name: String, email: String)

object User {
  implicit def userWrites: Writes[User] = (
    (JsPath \ "id").write[Long] and
      (JsPath \ "name").write[String] and
      (JsPath \ "email").write[String]
  )(unlift(User.unapply))
}
