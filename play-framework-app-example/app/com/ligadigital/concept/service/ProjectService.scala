package com.ligadigital.concept.service

import java.time.LocalDate

import com.google.inject.ImplementedBy
import com.ligadigital.concept.error._
import com.ligadigital.concept.message.{ProjectAlreadyExists, ProjectNotFound}
import com.ligadigital.concept.model.Project
import javax.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

@ImplementedBy(classOf[DefaultProjectService])
trait ProjectService {
  def create(authorId: Long, name: String, budget: Long, startDate: Option[LocalDate]): Future[Either[AppError, Project]]
  def find(id: Long): Future[Option[Project]]
  def findAll(): Future[List[Project]]
  def delete(id: Long): Future[Either[AppError, Unit]]
  def update(project: Project): Future[Either[AppError, Project]]
}

@Singleton
class DefaultProjectService @Inject() (userService: UserService)(implicit ec: ExecutionContext) extends ProjectService {
  private var projects = List(
    Project(1, "project #1", 1, LocalDate.now().minusMonths(1), 100, None),
    Project(2, "project #2", 3, LocalDate.now().minusMonths(3), 120, None),
    Project(3, "project #3", 2, LocalDate.now().minusMonths(2), 120, None)
  )

  override def create(authorId: Long, name: String, budget: Long, startDate: Option[LocalDate]): Future[Either[AppError, Project]] = Future {
    if (projects.exists(_.name == name)) {
      Left(ObjectAlreadyExistsError(ProjectAlreadyExists))
    } else {
      val newProject = Project(projects.size + 1, name, authorId, LocalDate.now(), budget, startDate)
      projects = newProject +: projects
      Right(newProject)
    }
  }

  override def find(id: Long): Future[Option[Project]] = Future {
    projects.find(_.id == id)
  }

  override def delete(id: Long): Future[Either[AppError, Unit]] = Future {
    if (!projects.exists(_.id == id)) {
      Left(NotFoundError(ProjectNotFound))
    } else {
      projects = projects.filterNot(_.id == id)
      Right(())
    }
  }

  override def update(project: Project): Future[Either[AppError, Project]] = Future {
    projects.find(_.id == project.id) match {
      case None =>
        Left(NotFoundError(ProjectNotFound))
      case Some(p) =>
        projects = project +: projects.filterNot(_ == p)
        Right(project)
    }
  }

  override def findAll(): Future[List[Project]] = Future {
    projects
  }
}
