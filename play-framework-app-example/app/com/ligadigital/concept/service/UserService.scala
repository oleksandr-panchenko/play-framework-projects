package com.ligadigital.concept.service

import com.google.inject.ImplementedBy
import com.ligadigital.concept.error.{AppError, ObjectAlreadyExistsError}
import com.ligadigital.concept.message.UserAlreadyExists
import com.ligadigital.concept.model.User
import javax.inject.{Inject, Singleton}

import scala.concurrent.{ExecutionContext, Future}

/**
  * @author oleksandr.panchenko@ligadigital.com
  */
@ImplementedBy(classOf[DefaultUserService])
trait UserService {
  def create(name: String, email: String): Future[Either[AppError, User]]
  def findAll(): Future[List[User]]
  def find(userId: Long): Future[Option[User]]
}

@Singleton
class DefaultUserService @Inject()(implicit ec: ExecutionContext) extends UserService {
  private var users = List(User(1, "user1", "user1@email.com"), User(3, "user3", "user3@email.com"), User(2, "user2", "user2@email.com"))

  override def create(name: String, email: String): Future[Either[AppError, User]] = Future {
    if (users.exists(_.email == email)) {
      Left(ObjectAlreadyExistsError(UserAlreadyExists))
    } else {
      val newUser = User(users.size + 1, name, email)
      users = newUser +: users
      Right(newUser)
    }
  }

  override def find(userId: Long): Future[Option[User]] = Future {
    users.find(_.id == userId)
  }

  override def findAll(): Future[List[User]] = Future(users)
}
